package com.example.bearapps.brandstofprijzen;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by BearApps on 28/11/2014.
 */
public class XMLParser {

    private static final String ns = null;

    /** This is the only function need to be called from outside the class */
    public List<HashMap<String, String>> parse(Reader reader)
            throws XmlPullParserException, IOException {
        try{
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(reader);
            parser.nextTag();
            return readStations(parser);
        }finally{
        }
    }

    private List<HashMap<String, String>> readStations(XmlPullParser parser)
            throws XmlPullParserException,IOException{

        List<HashMap<String, String>> list = new ArrayList<HashMap<String,String>>();

        parser.require(XmlPullParser.START_TAG, ns, "countries");

        while(parser.next() != XmlPullParser.END_TAG){
            if(parser.getEventType() != XmlPullParser.START_TAG){
                continue;
            }

            String name = parser.getName();
            if(name.equals("country")){
                list.add(readStation(parser));
            }
            else{
                skip(parser);
            }
        }
        return list;
    }

    private HashMap<String, String> readStation(XmlPullParser parser)
            throws XmlPullParserException, IOException{

        parser.require(XmlPullParser.START_TAG, ns, "country");

        //String countryName = parser.getAttributeValue(ns, "name");
        //String flag = parser.getAttributeValue(ns, "flag");
        String name="";
        String brand="";
        String adres="";
        String city="";
        String postalCode="";
        String super95Price="";
        String super98Price="";
        String dieselPrice="";
        String updateDate="";

        while(parser.next() != XmlPullParser.END_TAG){
            if(parser.getEventType() != XmlPullParser.START_TAG){
                continue;
            }

           // String name = parser.getName();

            if(name.equals("name")){
                name = readName(parser);
            }else if(name.equals("brand")){
                brand = parser.getAttributeValue(ns, "brand");
                readBrand(parser);
            }else if(name.equals("adres")) {
                adres = parser.getAttributeValue(ns, "adres");
                adres = readAdres(parser);
            }else if(name.equals("city")){
                city = parser.getAttributeValue(ns, "city");
                city = readAdres(parser);
            }else if(name.equals("postalcode")) {
                postalCode = parser.getAttributeValue(ns, "postalcode");
                postalCode = readPostalcode(parser);
            }else if(name.equals("super95price")) {
                super95Price = parser.getAttributeValue(ns, "super95price");
                super95Price = read95Price(parser);
            }else if(name.equals("super98price")) {
                super98Price = parser.getAttributeValue(ns, "super98price");
                super98Price = read98Price(parser);
            }else if(name.equals("dieselprice")) {
                dieselPrice = parser.getAttributeValue(ns, "dieselprice");
                dieselPrice = readDieselPrice(parser);
            }else if(name.equals("updatedate")) {
                updateDate = parser.getAttributeValue(ns, "updatedate");
                updateDate = readUpdateDate(parser);
            }else{
                skip(parser);
            }
        }

        String details =    "Name : " + name + "\n" +
                "Brand : " + brand + "\n" +
                "City : " + city + "(" + postalCode + ")";

        HashMap<String, String> hm = new HashMap<String, String>();
        hm.put("name", name);
        hm.put("super95price", super95Price);
        hm.put("super98price",super98Price);

        return hm;
    }


    /** Process Name tag in the xml data */
    private String readName(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "name");
        String name = readText(parser);
        return name;
    }

    /** Process Brand tag in the xml data */
    private String readBrand(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "brand");
        parser.nextTag();
        String brand = readText(parser);
        return brand;
    }

    /** Process Adres tag in the xml data */
    private String readAdres(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "adres");
        String adres = readText(parser);
        return adres;
    }

    /** Process City tag in the xml data */
    private String readCity(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "city");
        String city = readText(parser);
        return city;
    }

    /** Process Postalcode tag in the xml data */
    private String readPostalcode(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "postalcode");
        String postalcode = readText(parser);
        return postalcode;
    }

    /** Process super95price tag in the xml data */
    private String read95Price(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "super95price");
        String price95 = readText(parser);
        return price95;
    }

    /** Process super98price tag in the xml data */
    private String read98Price(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "super98price");
        String price98 = readText(parser);
        return price98;
    }

    /** Process dieselprice tag in the xml data */
    private String readDieselPrice(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "dieselprice");
        String dieselprice = readText(parser);
        return dieselprice;
    }

    /** Process updatedate tag in the xml data */
    private String readUpdateDate(XmlPullParser parser)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "updatedate");
        String updDate = readText(parser);
        return updDate;
    }

    /** Getting Text from an element */
    private String readText(XmlPullParser parser)
            throws IOException, XmlPullParserException{
        String result = "";
        if(parser.next()==XmlPullParser.TEXT){
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser)
            throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
